# Spring Cloud Ribbon

Ribbon是一个为客户端提供负载均衡功能的工具类，它内部提供了一个叫做ILoadBalance的接口代表负载均衡器的操作，比如有添加服务器操作、选择服务器操作、获取所有的服务器列表、获取可用的服务器列表等等。

### 使用

服务消费者直接通过调用被@LoadBalanced注解修饰过的RestTemplate来实现面向服务的接口调用。

负载均衡器是从服务发现组件(NacosDiscoveryClient或EurekaClient)（DiscoveryClient的实现类为NacosDiscoveryClient）获取服务信息，根据IRule去路由，并且根据IPing判断服务的可用性。

IRule主要有以下几个类型：

| 规则名称                 | 特点                                                         |
| ------------------------ | ------------------------------------------------------------ |
| BestAvailableRule        | 选择一个最小的并发请求的server，逐个考察server，如果Server被tripped了，则跳过 |
| RandomRule               | 随机选择一个Server                                           |
| WeightedResponseTimeRule | 根据响应时间加权，响应时间越长，权重越小，被选中的可能性越低 |
| RetryRule                | 对选定的负载均衡策略加上重试机制，在一个配置时间段内当选择Server不成功，则一直尝试使用subRule的方式选择一个可用的Server |
| RoundRobinRule           | 轮询选择，轮询index，选择index对应位置的Server               |
| ZoneAvoidanceRule        | **默认**的负载均衡策略，即复合判断Server所在区域的性能和Server的可用性选择Server，在没有区域的环境下，类似于轮询(RandomRule) |

只需简单的配置就能使用：

```
@Bean
public IRule ribbonRule() {
    return new BestAvailableRule();
}
```

细粒度的配置：

    // user-center是要调用的服务名
    @Configuration
    @RibbonClient(name="user-center",configuration = RibbonConfiguration.class)
    public class UserCenterRibbonConfiguration {
    }
    
    
    @Configuration
    public class RibbonConfiguration {
    	// 指定的负载均衡策略
        @Bean
        public IRule ribbonRule(){
            return new RandomRule();
        }
    }
或用配置：

```
# 通过配置文件指定user-center实例的ribbon负载均衡策略为RandomRule，和java代码方式指定效果一样
user-center:
  ribbon:
    NFLoadBalancerRuleClassName: com.netflix.loadbalancer.RandomRule
```



### 源码

RestTemplate是如何被改造的：

```
@ConditionalOnClass(RestTemplate.class)
@ConditionalOnBean(LoadBalancerClient.class)
public class LoadBalancerAutoConfiguration {
	// 1.获取到所有被loadBalanced修饰过的RestTemplate
    @LoadBalanced
    @Autowired(required=false)
    private List<RestTemplate> restTemplates = Collections.emptyList();
    
    // 2. 给RestTemplate准备拦截器
    @Bean
    public LoadBalancerInterceptor ribbonInterceptor(LoadBalancerClient 
    	loadBalancerClient) {
    	return new LoadBalancerInterceptor(loadBalancerClient);
    }
    
    // 3.准备Customizer，用于给RestTemplate增加拦截器
    @Bean
    @ConditionalOnMissingBean
    public RestTemplateCustomizer restTemplateCustomizer (
    	final LoadBalancerInterceptor loadBalancerInterceptor){
    	return new RestTemplateCustomizer() {
            @Override
            public void customize(RestTemplate restTemplate) {
                List<ClientHttpRequestInterceptor> list = new ArrayList<>(
             		restTemplate.getInterceptors());
                list.add(loadBalancerInterceptor);
                restTemplate.setInterceptors(list);
            }
        };
    }

    @Bean
    public SmartInitializingSingleton loadBalancedRestTemplateInitializer(
    	final List<RestTemplateCustomizer> customizers) {
        return new SmartInitializingSingleton() {
            @Override
            public void afterSingletonsInstantiated() {
                // 4.对RestTemplate添加拦截器
                for（RestTemplate restTemplate : restTemplates {
                    for(RestTemplateCustomizer customizer : customizers) {
                        customizer.customize（restTemplate）;
                    }
                }
            }
        }
    } 
}
```

根据以上代码所述，带着LoadBalance注解的RestTemplate在发起请求时被拦截器所代理，核心的代码实际上在LoadBalancerClient中。

而LoadBalancerClient中核心的代码是execute函数，在该函数中getLoadBalance方法获取LoadBalance，而LoadBalance是一个IloadBalance接口定义的具体实现，在该接口中定义了一个客户端负载均衡器需要的一系列抽象操作。

- addServers：向负载均衡器中维护的实例列表增加服务实例。
- chooseServer：通过某种策略，从负载均衡器中挑选出一个具体的服务实例。
- markServerDown：用来通知和标识负载均衡器中某个具体实例已经停止服务，不然负载均衡器在下一次获取服务实例清单前都会认为服务实例均是正常服务的。
- getReachableServers：获取当前正常服务的实例列表。
- getAllServers：获取所有已知的服务实例列表，包括正常服务和停止服务的实例。

且在RibbonClientConfiguration中定义了默认的ZoneAwareLoadBalancer实现类，如果有需要可通过重写定义Bean来复写或扩展该默认实现，同时可通过实现自定义的ILoadBalance接口来扩展实现。



在引入Spring Cloud Ribbon的依赖之后，就能够自动化构建下面这些接口的实现。

- IClientConfig:Ribbon 的客户端配置，默认采用 com.netflix.client.config.DefaultClientConfigImpl实现。
- IRule:Ribbon 的负载均衡策略，默认采用 com.netflix.loadbalancer.ZoneAvoidanceRule实现，该策略能够在多区域环境下选出最佳区域的实例进行访问。
- IPing:Ribbon的实例检查策略，默认采用com.netflix.loadbalancer.NoOpPing实现，该检查策略是一个特殊的实现，实际上它并不会检查实例是否可用，而是始终返回true，默认认为所有服务实例都是可用的。
- ServerList<Server>：服务实例清单的维护机制，默认采用 com.netflix.loadbalancer.ConfigurationBasedServerList实现。
- ServerListFilter<Server>：服务实例清单过滤机制，默认采用 org.springframework.cloud.netflix.ribbon.ZonePreferenceServerLis tFilter实现，该策略能够优先过滤出与请求调用方处于同区域的服务实例。
- LoadBalancer：负载均衡器，默认采用 com.netflix.loadbalancer.ZoneAwareLoadBalancer实现，它具备了区域感知的能力。

通过自动化配置的实现，我们可以轻松地实现客户端负载均衡。同时，针对一些个性化需求，我们也可以方便地替换上面的这些默认实现。只需在Spring Boot应用中创建对应的实现实例就能覆盖这些默认的配置实现。比如下面的配置内容。

### 参考

- https://blog.51cto.com/u_14014612/6010759