# Neural Network and Deep Learning

## 感知器

x：输入

w：权重

b：阈值偏置



感知器

神经元

S形神经元

二次代价函数：方差函数

梯度下降算法

反向传播

链式反则

交叉熵代价函数

过度拟合

深度神经网络

卷积神经网络：局部感受野、共享权重、混合

RNN

## 参考

- 梯度下降算法：https://www.zhihu.com/tardis/bd/ans/1639782992?source_id=1001
- 反向传播：https://www.zhihu.com/tardis/bd/art/459542886?source_id=1001
- 反向传播计算过程：https://www.cnblogs.com/codehome/p/9718611.html
- RNN：：https://zhuanlan.zhihu.com/p/30844905