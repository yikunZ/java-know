# elasticsearch

**正排索引和倒排索引的区别**

正排索引（Forward Index）和倒排索引（Inverted Index）是在信息检索领域中常用的两种索引结构，它们在索引数据和支持查询方面有着不同的方式和特点。

正排索引是根据文档的顺序构建的索引，它将文档中的每个单词或词组与其在文档中的位置进行关联。在正排索引中，每个文档都有一个唯一的标识符（如文档ID），并且可以按照这个标识符来查找和检索文档。正排索引可以提供文档级别的信息，例如文档的标题、作者、日期等，以及每个文档内部的结构和语义关系。在正排索引中，检索时需要遍历整个索引，查找匹配的文档。

倒排索引则是根据单词或词组构建的索引，它将每个单词或词组与包含它们的文档进行关联。在倒排索引中，每个单词或词组都有一个包含它们的文档列表，其中记录了出现该单词或词组的文档的标识符（如文档ID）。通过倒排索引，可以快速定位包含特定单词或词组的文档，并进行全文搜索、关键词查询等操作。倒排索引适用于大规模文本数据的检索，它提供了词级别的信息，允许用户根据关键词进行查询，并返回相关的文档。

因此，正排索引按照文档构建索引，适用于文档级别的操作和信息检索；而倒排索引按照单词构建索引，适用于词级别的操作和全文搜索。两者在索引构建和查询过程中的数据结构和查询性能方面有所不同，可以根据具体的应用场景选择使用。



**数据写入**

![写入数据](images/elasticsearch/写入数据.png)


以下是写单个文档所需的步骤：

1、客户端向 NODE I 发送写请求。

2、检查Active的Shard数。

3、NODEI 使用文档 ID 来确定文档属于分片 0，通过集群状态中的内容路由表信息获知分片 0 的主分片位于 NODE3 ，因此请求被转发到 NODE3 上。

4、NODE3 上的主分片执行写操作 。 如果写入成功，则它将请求并行转发到 NODE I 和

NODE2 的副分片上，等待返回结果 。当所有的副分片都报告成功， NODE3 将向协调节点报告

成功，协调节点再向客户端报告成功 。

5、在客户端收到成功响应时 ，意味着写操作已经在主分片和所有副分片都执行完成。



**数据写入详细流程**

![写入数据2](images/elasticsearch/写入数据2.png)


1、将document写入内存buffer缓存中,同时写入到translog中

2、每隔一秒钟，buffer中的数据被写入新的segment file，

3、同时进入os cache，此时index segment file被打开并供search使用,

4、buffer被清空

5、重复1~3，新的segment不断添加，buffer不断被清空，而translog中的数据不断累加

6、当translog长度达到一定程度的时候，commit操作发生

  （6-1）buffer中的所有数据写入一个新的segment，并写入os cache，打开供使用

  （6-2）buffer被清空

  （6-3）一个commit ponit被写入磁盘，标明了所有的index segment

  （6-4）filesystem cache中的所有index segment file缓存数据，被fsync强行刷到磁盘上

7、现有的translog被清空，创建一个新的translog



**refresh 操作**
        primary shard 主分片先将数据写入 memory buffer，然后定时（默认每隔1s）将 memory buffer 中的数据写入一个新的 segment 文件中，并进入 Filesystem cache（同时清空 memory buffer），这个过程就叫做 refresh；每个 Segment 文件实际上是一些倒排索引的集合， 只有经历了 refresh 操作之后，这些数据才能变成可检索的。

ES 的近实时性：当数据存在 memory buffer 时是搜索不到的，只有数据被 refresh 到  Filesystem cache 之后才能被搜索到，而 refresh 是每秒一次， 所以称 es 是近实时的，或者可以通过手动调用 es 的 api 触发一次 refresh 操作，让数据马上可以被搜索到；

    上文讲到的 memory buffer，也称为 Indexing Buffer，这个区域默认的内存大小是 10% heap size。



**写 translog 事务日志文件**
        由于 memory Buffer 和 Filesystem Cache 都是基于内存，假设服务器宕机，那么数据就会丢失，所以 ES 通过 translog 日志文件来保证数据的可靠性，在数据写入 memory buffer 的同时，将数据写入 translog 日志文件中，在机器宕机重启时，es 会从磁盘中读取 translog 日志文件中最后一个提交点 commit point 之后的数据，恢复到 memory buffer 和 Filesystem cache 中去。

ES 数据丢失的问题：translog 也是先写入 Filesystem cache，然后默认每隔 5 秒刷一次到磁盘中，所以默认情况下，可能有 5 秒的数据会仅仅停留在 memory buffer 或者 translog 文件的 Filesystem cache中，而不在磁盘上，如果此时机器宕机，会丢失 5 秒钟的数据。也可以将 translog 设置成每次写操作必须是直接 fsync 到磁盘，但是性能会差很多。



**flush 操作**
        不断重复上面的步骤，translog 会变得越来越大，当 translog 文件默认每30分钟或者阈值超过 512M 时，就会触发 flush 操作，将 memory buffer 中所有的数据写入新的 Segment 文件中， 并将内存中所有的 Segment 文件全部落盘，最后清空 translog 事务日志。

（1）将 memory buffer 中的数据 refresh 到 Filesystem Cache 中的一个新的 segment 文件中去，然后清空 memory buffer；
（2）创建一个新的 commit point（提交点），同时强行将 Filesystem Cache 中目前所有的数据都 fsync 到磁盘文件中；
（3）删除旧的 translog 日志文件并创建一个新的 translog 日志文件，此时 flush 操作完成
ES 的 flush 操作主要通过以下几个参数控制：

```
index.translog.flush_threshold_period：每隔多长时间执行一次flush，默认30m
index.translog.flush_threshold_size：当事务日志大小到达此预设值，则执行flush，默认512mb
index.translog.flush_threshold_ops：当事务日志累积到多少条数据后flush一次。
```



## BM25

BM25算法相对于传统的TF/IDF算法进行了以下改进：

1. 考虑词频饱和度：传统的TF/IDF算法假设词频与相关性成正比，但在实际情况中，词频存在饱和现象，即随着词频的增加，对相关性的影响递减。BM25算法引入了一个饱和函数k1，使得词频的增加对得分的影响递减，更符合实际情况。
2. 考虑文档长度：传统的TF/IDF算法没有考虑文档的长度对相关性得分的影响。BM25算法引入了参数b来控制文档长度的影响程度，较长的文档对相关性得分的贡献更大，从而更准确地衡量文档的相关性。
3. 考虑文档频率的逆文档频率（IDF）：与传统的TF/IDF算法一样，BM25算法也使用了IDF的概念。IDF衡量了一个词在整个文档集中的重要程度，用于对词频进行修正，以提高检索结果的准确性。
4. 考虑平均文档长度：BM25算法引入了一个平均文档长度的参数，用于对文档长度的影响进行归一化。这样可以在不同长度的文档之间进行公平的比较，并使得算法更具有普适性。

总体而言，BM25算法通过引入饱和函数、文档长度调整和平均文档长度归一化等改进，更准确地估计文档的相关性，提高了信息检索的效果。

算法参数：

- k1：词频对得分的影响大小，更高的k1值导致词频更大的文档会有更高的得分，默认为1.2。

- b：文档长度对得分的影响大小，更高的b值导致更长的文档有更高的得分，默认为0.75。

  

## 参考

- https://blog.csdn.net/wangguoqing_it/article/details/121389185
- https://blog.csdn.net/a745233700/article/details/118076845