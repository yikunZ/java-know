# JVM垃圾回收

参考JavaGuide：https://github.com/Snailclimb/JavaGuide/blob/main/docs/java/jvm/jvm-garbage-collection.md



## 补充

问：Parallel Scavenge收集器为何无法与CMS同时使用？

答：https://www.zhihu.com/question/43492129

https://hllvm-group.iteye.com/group/topic/37095#post-242695

https://www.jianshu.com/p/8ffc23c34d70



## 总结

### 引用计数算法

在对象中添加一个引用计数器，每当有一个地方引用它时，计数器值就加一；当引用失效时，计数器值就减一；任何时刻计数器为零的对象就是不可能再被使用的。**很难解决对象之间相互循环引用的问题**，因此很少使用。



### 可达性分析算法

通过 一系列称为“GC Roots”的根对象作为起始节点集，从这些节点开始，根据引用关系向下搜索，搜索过程所走过的路径称为“引用链”（Reference Chain），如果某个对象到GC Roots间没有任何引用链相连， 或者用图论的话来说就是从GC Roots到这个对象不可达时，则证明此对象是不可能再被使用的。

GC Roots的对象包括以下几种： 

- 在虚拟机栈（栈帧中的本地变量表）中引用的对象，譬如各个线程被调用的方法堆栈中使用到的参数、局部变量、临时变量等。 

- 在方法区中类静态属性引用的对象，譬如Java类的引用类型静态变量。 

- 在方法区中常量引用的对象，譬如字符串常量池（String Table）里的引用。
- 在本地方法栈中JNI（即通常所说的Native方法）引用的对象。 

- Java虚拟机内部的引用，如基本数据类型对应的Class对象，一些常驻的异常对象（比如NullPointExcepiton、OutOfMemoryError）等，还有系统类加载器。 

- 所有被同步锁（synchronized关键字）持有的对象。 

- 反映Java虚拟机内部情况的JMXBean、JVMTI中注册的回调、本地代码缓存等。

- 其他“临时性”对象：在分代GC算法中，如果老年代引用新生代对象，在对新生代进行GC时要将这些引用了新生代的老年代对象考虑进去。

  

### 引用

- 强引用
- 软应用：SoftReference类实现，在系统将要发生内存溢出异常前，会把这些对象列进回收范围之中进行第二次回收，如果这次回收还没有足够的内存，才会抛出内存溢出异常。
- 弱应用：WeakReference类实现，GC发生时，就会回收。
- 虚引用：PhantomReference类实现，为一个对象设置虚引用关联的唯一目的只是为了能在这个对象被收集器回收时收到一个系统通知。



### finalize

- 在可达性分析之后，会将可GC的对象进行第一次标记
- 然后进行筛选，筛选的条件是此对象是否有必要执行finalize()方法。假如对象没有覆盖finalize()方法，或者finalize()方法已经被虚拟机调用过，那么虚拟机将这两种情况都视为“没有必要执行”。
- 对需要执行finalize方法的对象（对象存放在F-Queue队列中）执行一次finalize方法
  - 如果finalize方法中重新与引用链上的任何一个对象建立关联，那么就不回收
  - 否则进行二次标记为回收

**注意**：finalize对象从始至终只会被执行一次，官方不建议使用它，完全可以用try-finally方式代替。



### 方法区回收

回收对象是废弃常量和类型不再使用的类型。

判断一个类不再使用的条件：

- 类实例都被回收了
- 该类的类加载器被回收了
- 该类的Class对象没有引用



### 收集理论

- 弱分代假说（Weak Generational Hypothesis）：绝大多数对象都是朝生夕灭的，70%到99%的对象可被一次GC就进行收集。
- 强分代假说（Strong Generational Hypothesis）：熬过越多次垃圾收集过程的对象就越难以消亡。
- 跨代引用假说（Intergenerational Reference Hypothesis）：跨代引用相对于同代引用来说仅占极少数。

需在新生代上建立一个全局的数据结构（该结构被称为“记忆集”，Remembered Set），这个结构把老年代划分成若干小块，标识出老年代的哪一块内存会存在跨代引用。此后当发生Minor GC时，只有包含了跨代引用的小块内存里的对象才会被加入到GCRoots进行扫描。



GC划分

- 部分收集（Partial GC）：指目标不是完整收集整个Java堆的垃圾收集，其中又分为： 
  - 新生代收集（Minor GC/Young GC）：指目标只是新生代的垃圾收集。 
  - 老年代收集（Major GC/Old GC）：指目标只是老年代的垃圾收集。目前只有CMS收集器会有单独收集老年代的行为。
  - 混合收集（Mixed GC）：指目标是收集整个新生代以及部分老年代的垃圾收集。目前只有G1收集器会有这种行为。 

- 整堆收集（Full GC）：收集整个Java堆和方法区的垃圾收集。



### GC算法

#### 标记-清除

它是最基础的算法，是因为后续的收集算法大多都是以标记-清除算法为基础，对其缺点进行改进而得到的。

它的主要缺点有两个：

- 第一个是执行效率不稳定：如果Java堆中包含大量对象，而且其中大部分是需要被回收的，这时必须进行大量标记和清除的动作，导致标记和清除两个过程的执行效率都随对象数量增长而降低。
- 第二个是内存空间的碎片化问题：标记、清除之后会产生大 量不连续的内存碎片，空间碎片太多可能会导致当以后在程序运行过程中需要分配较大对象时无法找到足够的连续内存而不得不提前触发另一次垃圾收集动作。



#### 标记-复制

**为了解决标记-清除算法面对大量可回收对象时执行效率低的问题**

它将可用内存按容量划分为大小相等的两块，每次只使用其中的一块。当这一块的内存用完了，就将还存活着的对象复制到另外一块上面，然后再把已使用过的内存空间一次清理掉。

**优化**

根据对象“朝生夕灭”特点，提出Appel式回收：把新生代分为一块较大的Eden空间和两块较小的 Survivor空间，每次分配内存只使用Eden和其中一块Survivor。发生垃圾搜集时，将Eden和Survivor中仍然存活的对象一次性复制到另外一块Survivor空间上，然后直接清理掉Eden和已用过的那块Survivor空间。HotSpot虚拟机默认Eden和Survivor的大小比例是8∶1，也即每次新生代中可用内存空间为整个新生代容量的90%（Eden的80%加上一个Survivor的10%），只有一个Survivor空间，即10%的新生代是会被“浪费”的。**HotSpot虚拟机的Serial、ParNew等新生代收集器均采用了这种策略来设计新生代的内存布局**。

**分配担保**

如果另外一块 Survivor空间没有足够空间存放上一次新生代收集下来的存活对象，这些对象便将通过分配担保机制直接进入老年代，这对虚拟机来说就是安全的。

原因：98%的对象可被回收仅仅是“普通场景”下测得的数据，任何人都没有办法百分百保证每次回收都只有不多于10%的对象存活，因此Appel式回收还有一个充当罕见情况的“逃生门”的安全设计，当Survivor空间不足以容纳一次Minor GC之后存活的对象时，就需要依赖其他内存区域（实 际上大多就是老年代）进行分配担保（Handle Promotion）。



#### 标记-整理

为了解决内存碎片化问题，以及标记-清除算法适用对象存活率高的情况，所以在老年代不适用。

标记-清除算法与标记-整理算法的本质差异在于前者是一种非移动式的回收算法，而后者是移动式的。是否移动回收后的存活对象是一项优缺点并存的风险决策：

- 如果移动存活对象，尤其是在老年代这种每次回收都有大量对象存活区域，移动存活对象并更新所有引用这些对象的地方将会是一种极为负重的操作，而且这种对象移动操作必须全程暂停用户应用程序才能进行，这就更加让使用者不得不小心翼翼地权衡其弊端了，像这样的停顿被最初的虚拟机设计者形象地描述为“Stop The World”。 
- 如果不移动，弥散于堆中的存活对象导致的 空间碎片化问题就只能依赖更为复杂的内存分配器和内存访问器来解决。譬如通过“分区空闲分配链表”来解决内存分配问题。内存的访问是用户程序最频繁的操作，甚至都没有之一，假如在这个环节上增加了额外的负担，势必会直接影响应用程序的吞吐量。 
- 移动则内存回收时会更复杂，不移动则内存分配时会更复杂。从垃圾收集的停顿时间来看，不移动对象停顿时间会更短，甚至可以不需要停顿，但是从整个程序的吞吐量来看，移动对象会更划算。

HotSpot虚拟机里面关注吞吐量的Parallel Old收集器是基于标记-整理算法的，而关注延迟的CMS收集器则是基于标记-清除算法的。Java 8默认使用Parallel Scavenge+parallel Old进行垃圾收集，若想最求更短的停顿时间可使用CMS+ParNew。Java 8之后提出新的垃圾收集器，例如G1、ZGC更多的都是追求更短的停顿时间为目标。Java 9默认的垃圾收集器是G1，它与CMS的关注点是一致的。



![image-20211117165638414](images\JVM垃圾回收\垃圾收集器.jpg)


GC全过程

![img](images\JVM垃圾回收\gc过程.png)



### 实战案例

**JVM调优案例1**
Phoenix Query Server是一个Java程序，发现该程序在使用过程中卡顿非常严重，通过使用jstat -gcutil和jsat -heap命令查看到该程序新生代内存增长非常快，尤其是Eden区，
且频繁触发Young GC，且在每次触发Young GC后在老年代会产生大量占用内存，然后在经过几次Young GC后，老年代内存爆满，又会触发Full GC。
这种情况暗示了其实是新生代内存不足导致的，新生代内存不足，导致频繁触发Young GC，当新生代内存不够又需要申请新内存时，可能直接在老年代申请内存。
那么我们应当扩大新生代的内存分配，通过调整newRatio参数来调节新生代内存大小，调节SurvivorRatio参数来调节新生代Eden区的内存大小，通过这样的修改后，Full GC的频率由原来的10秒一次降低到一天两次，Young GC也由原来的3-5秒一次降低到1分钟一次。



**JVM调优案例2**

中台的搜索服务在某天突然出现非常高频的内存溢出，导致服务不可用，通过对服务添加参数：PrintGCDetails、HeapDumpOnOutOfMemoryError、jmap命令打印内存信息文件，再通过EMA（eclipse memory analysis）工具对文件内容进行分析，发现定位到在使用到的第三方es客户端工具包中的某个类的内存占比特别高，通过对源码分析发现该工具类内部对es查询参数做了缓存处理，通过了解发现该服务对外提供了一个查询接口，没有对查询参数个数进行限制，被其他部门的服务误用传输大量的参数进来被工具缓存，而该es缓存参数没有做弱引用处理导致占用大量内存，通过查阅该工具的官方文档发现可以禁用改缓存，于是设置参数后再也没有出现过内存溢出的问题。



**JVM调优案例3**

某个服务产生有大量的线程以及占用很高的内存，使用jstack命令查看堆栈信息，发现有大量线程Runable在某个用户代码中，最终定位到是访问mysql，结合mysql慢日志以及代码分析，发现是limit没有生效导致的整表扫描。